FROM gcr.io/distroless/java17-debian11
WORKDIR /project

COPY build/libs/transaction-service-1.0.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]