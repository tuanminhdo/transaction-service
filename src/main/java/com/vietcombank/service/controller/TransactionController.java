package com.vietcombank.service.controller;

import com.vietcombank.service.entity.TransactionEntity;
import com.vietcombank.service.model.CreateTransactionReq;
import com.vietcombank.service.service.TransactionService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/api")
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @GetMapping(value = "/v1/transactions", produces = "application/json")
    @Operation(summary = "Retrieve All Transaction")
    public ResponseEntity<List<TransactionEntity>> retrieveTransactions() {
        log.info("retrieve all transactions");
        try {
            return ResponseEntity.ok(transactionService.retrieveAll());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping(value = "/v1/transaction/{id}", produces = "application/json")
    @Operation(summary = "Retrieve Transaction by Id")
    public ResponseEntity<TransactionEntity> retrieveTransactionById(@PathVariable UUID id) {
        log.info("retrieve transaction by id");
        try {
            Optional<TransactionEntity> trans = transactionService.findById(id);
            if(trans.isPresent()) {
                return ResponseEntity.ok(trans.get());
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping(value = "/v1/transaction", produces = "application/json")
    @Operation(summary = "Create Transaction")
    public ResponseEntity<TransactionEntity> create(@RequestBody CreateTransactionReq req) {
        log.info("create transaction");
        try {
            TransactionEntity trans = new TransactionEntity();
            trans.setId(UUID.randomUUID());
            trans.setFromUser(req.getFromUser());
            trans.setToUser(req.getToUser());
            trans.setAmount(req.getAmount());
            trans.setCreatedAt(Instant.now());
            trans.setUpdatedAt(null);

            transactionService.insert(trans);
            return ResponseEntity.ok(trans);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }
}
