package com.vietcombank.service.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "TRANSACTIONS")
@Data
public class TransactionEntity {
    @Id
    @Column(name = "ID", nullable = false)
    private UUID id;

    @Column(name = "TO_USER")
    private String toUser;

    @Column(name = "FROM_USER")
    private String fromUser;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "CREATED_AT")
    private Instant createdAt;

    @Column(name = "UPDATED_AT")
    private Instant updatedAt;
}