package com.vietcombank.service.model;

import jakarta.persistence.Column;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CreateTransactionReq {
    private String fromUser;
    private String toUser;
    private BigDecimal amount;
}
