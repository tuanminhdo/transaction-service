package com.vietcombank.service.service;

import com.vietcombank.service.entity.TransactionEntity;
import com.vietcombank.service.repository.TransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class TransactionService {
    @Autowired
    private TransactionRepository transactionEntityRepository;

    public List<TransactionEntity> retrieveAll() {
        return transactionEntityRepository.findAll();
    }

    public Optional<TransactionEntity> findById(UUID id) {
        return transactionEntityRepository.findById(id);
    }

    public TransactionEntity insert(TransactionEntity entity) {
        return transactionEntityRepository.save(entity);
    }
}
