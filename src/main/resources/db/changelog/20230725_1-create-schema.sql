--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE TRANSACTIONS
(
    ID                  uuid PRIMARY KEY default uuid_generate_v4(),
    FROM_USER           varchar(255),
    TO_USER             varchar(255),
    AMOUNT              decimal,
    CREATED_AT          timestamp,
    UPDATED_AT          timestamp
);