--liquibase formatted sql
--changeset microservice_class:20230725_2

INSERT INTO TRANSACTIONS
(id, FROM_USER,TO_USER,AMOUNT, CREATED_AT, UPDATED_AT)
VALUES ('6364bf5d-fa89-4ab3-b274-1da8cdb785b3', 'user01','user02',2000000, '2023-07-25 11:34:53.629',null),
       ('66620d36-6a37-4ce3-9db0-783ee3fd11b4', 'user02','user01',1000000, '2023-07-25 11:35:53.629',null);